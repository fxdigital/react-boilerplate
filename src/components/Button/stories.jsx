import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { text } from "@storybook/addon-knobs";
import Button from "./index";

const stories = storiesOf("Button", module);

stories.addParameters({
  info: {
    text: `An example button.`
  }
});

stories.add("with text", () => (
  <Button onClick={action("clicked")}>{text("Name", "I am a button")}</Button>
));

stories.add("with some emoji", () => (
  <Button onClick={action("clicked")}>
    <span role="img" aria-label="so cool">
      😀 😎 👍 💯
    </span>
  </Button>
));
