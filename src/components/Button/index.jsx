import React from "react";
import PropTypes from "prop-types";
import styles from "./styles.scss";

const Button = ({ children, onClick }) => (
  <button onClick={onClick} className={styles.foo} type="button">
    {children}
  </button>
);

Button.defaultProps = {
  onClick: null
};

if (__DEV__) {
  Button.propTypes = {
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func
  };
}

export default Button;
