import React from "react";

import css from "./style.css";
import less from "./style.less";
import scss from "./style.scss";
import Button from "./components/Button";

const App = () => {
  return (
    <>
      <div>Hello World</div>

      <div className={css.foo}>CSS</div>
      <div className={less.foo}>LESS</div>
      <div className={scss.foo}>SCSS</div>

      <Button>I am a button</Button>

      <Button>
        <span role="img" aria-label="so cool">
          😀 😎 👍 💯
        </span>
      </Button>
    </>
  );
};

export default App;
