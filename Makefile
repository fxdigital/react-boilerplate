nbin = ./node_modules/.bin
eslint = $(nbin)/eslint . --ext .js,.jsx,.mjs --format=codeframe
prettier = $(nbin)/prettier "./**/*.{css,graphql,js,json,jsx,md,mjs,scss,yml,yaml}"
stylelint = $(nbin)/stylelint "./**/*.{css,less,scss}"

build:
	rm -rf ./dist
	NODE_ENV=production $(nbin)/webpack

commit:
	 $(nbin)/git-cz

format:
	$(prettier) --write
	$(eslint) --fix
	$(stylelint) --fix

lint:
	$(prettier) --check
	$(eslint)
	$(stylelint)

storybook:
	$(nbin)/start-storybook -p 8081 -c .storybook

watch:
	NODE_ENV=development $(nbin)/webpack-dev-server

ci-lint:
	$(prettier) --check || TEST_EC=$${?}; \
	$(eslint) || TEST_EC=$${?}; \
	$(stylelint) || TEST_EC=$${?}; \
	exit $${TEST_EC}