const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const {
  IS_PRODUCTION,
  IS_DEVELOPMENT,
  REGEX_CSS,
  REGEX_LESS,
  REGEX_SASS
} = require("./constants");

const common = (preprocessors = []) =>
  [
    IS_DEVELOPMENT && "style-loader",
    IS_PRODUCTION && MiniCssExtractPlugin.loader,
    {
      loader: "css-loader",
      options: {
        modules: {
          mode: "local",
          localIdentName: "[name]__[local]___[hash:base64:5]"
        },
        importLoaders: preprocessors.length + 1,
        sourceMap: IS_DEVELOPMENT
      }
    },
    {
      loader: "postcss-loader",
      options: {
        ident: "postcss",
        plugins: () => [
          require("postcss-flexbugs-fixes"),
          require("postcss-preset-env")({
            autoprefixer: {
              flexbox: "no-2009"
            },
            stage: 3
          })
        ],
        sourceMap: IS_DEVELOPMENT
      }
    },
    ...preprocessors
  ].filter(Boolean);

module.exports = [
  {
    test: REGEX_CSS,
    sideEffects: true,
    use: common()
  },
  {
    test: REGEX_SASS,
    sideEffects: true,
    use: common([
      {
        loader: "sass-loader",
        options: {
          sourceMap: IS_DEVELOPMENT
        }
      }
    ])
  },
  {
    test: REGEX_LESS,
    sideEffects: true,
    use: common([
      {
        loader: "less-loader",
        options: {
          sourceMap: IS_DEVELOPMENT
        }
      }
    ])
  }
];
