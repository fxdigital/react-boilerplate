const {
  IS_DEVELOPMENT,
  REGEX_STYLE,
  REGEX_SCRIPT,
  REGEX_IMG
} = require("./constants");

const staticAssetName = IS_DEVELOPMENT
  ? "static/assets/[path][name].[ext]?[hash]"
  : "static/assets/[hash].[ext]";

module.exports = [
  {
    test: /\.txt$/,
    loader: "raw-loader"
  },
  {
    exclude: [
      REGEX_STYLE,
      REGEX_SCRIPT,
      REGEX_IMG,
      /\.json$/,
      /\.txt$/,
      /\.ejs$/
    ],
    loader: "file-loader",
    options: {
      name: staticAssetName
    }
  }
];
