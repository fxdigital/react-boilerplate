const { IS_PRODUCTION, IS_DEVELOMENT, REGEX_SCRIPT } = require("./constants");

module.exports = [
  {
    test: REGEX_SCRIPT,
    exclude: /(node_modules|bower_components)/,
    loader: "babel-loader",
    options: {
      cacheDirectory: IS_DEVELOMENT,
      babelrc: false,
      configFile: false,
      presets: [
        [
          "airbnb",
          {
            removePropTypes: IS_PRODUCTION
          }
        ]
      ],
      plugins: [
        ...(IS_PRODUCTION ? ["@babel/transform-react-constant-elements"] : []),
        ...(IS_PRODUCTION ? ["@babel/transform-react-inline-elements"] : [])
      ]
    }
  }
];
