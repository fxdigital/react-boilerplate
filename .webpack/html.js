const { resolve } = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ManifestPlugin = require("webpack-manifest-plugin");
const ScriptExtHtmlWebpackPlugin = require("script-ext-html-webpack-plugin");
const { IS_PRODUCTION, IS_DEVELOPMENT, ROOT_DIR } = require("./constants");

module.exports = [
  new HtmlWebpackPlugin({
    title: "Sample App",
    inject: "head",
    showErrors: IS_DEVELOPMENT,
    minify: IS_PRODUCTION
      ? {
          removeComments: true,
          collapseWhitespace: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeStyleLinkTypeAttributes: true,
          keepClosingSlash: true,
          minifyJS: true,
          minifyCSS: true,
          minifyURLs: true
        }
      : false,
    template: resolve(ROOT_DIR, "./html.ejs"),
    filename: "./index.html",

    // custom properties
    preconnect: []
  }),
  new ScriptExtHtmlWebpackPlugin({
    defaultAttribute: "async"
  }),
  new ManifestPlugin({
    fileName: "static/assets-manifest.json"
  })
];
