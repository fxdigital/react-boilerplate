const { resolve } = require("path");
const { DefinePlugin, HotModuleReplacementPlugin } = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ModuleScopePlugin = require("./ModuleScopePlugin");
const {
  ENV,
  IS_PRODUCTION,
  IS_DEVELOPMENT,
  ROOT_DIR,
  DIST_DIR,
  SRC_DIR
} = require("./constants");

module.exports = {
  context: ROOT_DIR,
  entry: "./src/entry.jsx",
  mode: ENV,
  devtool: IS_DEVELOPMENT ? "source-map" : false,
  bail: IS_PRODUCTION,
  output: {
    path: DIST_DIR,
    pathinfo: IS_DEVELOPMENT,
    filename: "static/script/[name].[hash].bundle.js",
    chunkFilename: "static/script/[name].[hash].chunk.js",
    sourceMapFilename: "static/script/[name].[hash].bundle.js.map",
    devtoolModuleFilenameTemplate: info =>
      resolve(info.absoluteResourcePath).replace(/\\/g, "/")
  },
  resolve: {
    extensions: [".mjs", ".js", ".jsx", ".json"],
    plugins: [
      new ModuleScopePlugin(SRC_DIR, [resolve(ROOT_DIR, "./package.json")])
    ]
  },
  module: {
    strictExportPresence: true,
    rules: [
      { parser: { requireEnsure: false } },
      ...require("./babel"),
      ...require("./styles"),
      ...require("./images"),
      ...require("./other")
    ]
  },
  node: {
    fs: "empty",
    net: "empty",
    tls: "empty"
  },
  optimization: require("./optimization.js"),
  plugins: [
    new DefinePlugin({
      "process.env.BROWSER": true,
      __DEV__: IS_DEVELOPMENT
    }),
    IS_PRODUCTION &&
      new MiniCssExtractPlugin({
        filename: "static/style/[name].[hash].css",
        chunkFilename: "static/style/[name].[hash].chunk.css"
      }),
    IS_DEVELOPMENT && new HotModuleReplacementPlugin(),
    ...require("./html")
  ].filter(Boolean),
  devServer: require("./devServer")
};
