const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const safePostCssParser = require("postcss-safe-parser");
const TerserPlugin = require("terser-webpack-plugin");
const { IS_PRODUCTION } = require("./constants");

module.exports = {
  minimize: IS_PRODUCTION,
  minimizer: [
    new TerserPlugin({
      terserOptions: {
        parse: {
          ecma: 8
        },
        compress: {
          ecma: 5,
          warnings: false,
          comparisons: false,
          inline: 2
        },
        mangle: {
          safari10: true
        },
        output: {
          ecma: 5,
          comments: false,
          ascii_only: true
        }
      },
      parallel: true,
      cache: true,
      sourceMap: false
    }),
    new OptimizeCSSAssetsPlugin({
      cssProcessorOptions: {
        parser: safePostCssParser,
        map: false
      }
    })
  ],
  splitChunks: {
    cacheGroups: {
      commons: {
        chunks: "initial",
        test: /[\\/]node_modules[\\/]/,
        name: "vendors"
      }
    }
  }
};
