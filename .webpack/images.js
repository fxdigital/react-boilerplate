const { IS_DEVELOPMENT, REGEX_IMG, REGEX_STYLE } = require("./constants");

const staticAssetName = IS_DEVELOPMENT
  ? "static/media/[path][name].[ext]?[hash]"
  : "static/media/[hash].[ext]";

module.exports = [
  {
    test: REGEX_IMG,
    oneOf: [
      {
        issuer: REGEX_STYLE,
        oneOf: [
          {
            test: /\.svg$/,
            loader: "svg-url-loader",
            options: {
              name: staticAssetName,
              limit: 4096 // 4kb
            }
          },
          {
            loader: "url-loader",
            options: {
              name: staticAssetName,
              limit: 4096 // 4kb
            }
          }
        ]
      },
      {
        loader: "file-loader",
        options: {
          name: staticAssetName
        }
      }
    ]
  }
];
