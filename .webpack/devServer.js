const { IS_PRODUCTION, DIST_DIR } = require("./constants");

module.exports = {
  inline: true,
  compress: IS_PRODUCTION,
  contentBase: DIST_DIR,
  disableHostCheck: true,
  historyApiFallback: true,
  host: "0.0.0.0",
  hot: true,
  port: 8080,
  watchOptions: {
    poll: 1000
  }
};
