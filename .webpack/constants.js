const { resolve } = require("path");

const IS_PRODUCTION = process.env.NODE_ENV === "production";
const IS_DEVELOPMENT = !IS_PRODUCTION;
const ENV = IS_PRODUCTION ? "production" : "development";

const ROOT_DIR = resolve(__dirname, "..");
const SRC_DIR = resolve(ROOT_DIR, "./src");
const DIST_DIR = resolve(ROOT_DIR, "./dist");

const REGEX_IMG = /\.(bmp|gif|jpg|jpeg|png|svg)$/;
const REGEX_SCRIPT = /\.(js|jsx|mjs)$/;
const REGEX_STYLE = /\.(css|less|scss)$/;
const REGEX_CSS = /\.css$/;
const REGEX_SASS = /\.scss$/;
const REGEX_LESS = /\.less$/;

module.exports = {
  ENV,
  IS_DEVELOPMENT,
  IS_PRODUCTION,

  ROOT_DIR,
  SRC_DIR,
  DIST_DIR,

  REGEX_IMG,
  REGEX_SCRIPT,
  REGEX_STYLE,
  REGEX_CSS,
  REGEX_SASS,
  REGEX_LESS
};
