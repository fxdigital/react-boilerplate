module.exports = {
  root: true,
  extends: ["@fxdigital/fxdigital/react"],
  overrides: [
    {
      files: ["src/components/**/stories.jsx"],
      rules: {
        "import/no-extraneous-dependencies": "off"
      }
    },
    {
      files: [".storybook/*.js", ".webpack/*.js"],
      rules: {
        "import/no-extraneous-dependencies": "off",
        "global-require": "off"
      }
    }
  ]
};
