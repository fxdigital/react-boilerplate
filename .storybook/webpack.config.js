const createCompiler = require("@storybook/addon-docs/mdx-compiler-plugin");
const { DefinePlugin } = require("webpack");

module.exports = {
  module: {
    rules: [
      ...require("../.webpack/styles"),
      ...require("../.webpack/images"),
      ...require("../.webpack/other"),
      {
        test: /\.(stories|story)\.mdx$/,
        use: [
          {
            loader: "babel-loader",
            // may or may not need this line depending on your app's setup
            options: {
              plugins: ["@babel/plugin-transform-react-jsx"]
            }
          },
          {
            loader: "@mdx-js/loader",
            options: {
              compilers: [createCompiler({})]
            }
          }
        ]
      },
      {
        test: /\.(stories|story)\.[tj]sx?$/,
        loader: require.resolve("@storybook/source-loader"),
        exclude: [/node_modules/],
        enforce: "pre"
      }
    ]
  },
  plugins: [
    new DefinePlugin({
      __DEV__: true
    })
  ]
};
