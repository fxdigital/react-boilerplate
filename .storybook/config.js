import { addDecorator, addParameters, configure } from "@storybook/react";
import { withA11y } from "@storybook/addon-a11y";
import { DocsPage, DocsContainer } from "@storybook/addon-docs/blocks";
import { withKnobs } from "@storybook/addon-knobs";

const req = require.context("../src/components", true, /stories\.jsx?$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

addParameters({
  docs: {
    container: DocsContainer,
    page: DocsPage
  }
});

addDecorator(withA11y);
addDecorator(withKnobs);

configure(loadStories, module);
