# React App Template

## Commands

| Command          | Description                                                        |
| ---------------- | ------------------------------------------------------------------ |
| `yarn`           | Install dependencies.                                              |
| `yarn build`     | Create production bundle in `/dist` directory.                     |
| `yarn commit`    | Commit using commitizen commit message helper.                     |
| `yarn format`    | Reformat code with prettier, eslint, & stylelint.                  |
| `yarn lint`      | Check code style for errors with prettier, eslint, & stylelint.    |
| `yarn storybook` | Run storybook component library on port `8081`.                    |
| `yarn watch`     | Run webpack dev server on port `8080` with hot module replacement. |
